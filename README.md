# [iris-keymap](https://gitlab.com/eidoom/iris-keymap)

My personal [Iris keymap](https://github.com/qmk/qmk_firmware/tree/master/keyboards/keebio/iris/keymaps)

## Flashing

Install [my fork](https://gitlab.com/eidoom/qmk_firmware/-/blob/master/README-FORK.md) then
```bash
qmk flash -kb keebio/iris/rev3 -km eidoom
```
then on the keyboard either
* left `Mod` (`LOWER`) and `Tab`
* right `Mod` (`RAISE`) and `\` (mirror of previous)

## Old method

### Install

```shell
cd ~/git
git clone --recurse-submodules git@github.com:qmk/qmk_firmware.git
cd ~/git/qmk_firmware
./util/qmk_install.sh
cd ~/git/qmk_firmware/keyboards/keebio/iris/keymaps
git clone git@gitlab.com:eidoom/iris-keymap.git eidoom
```

### Update

```shell
cd ~/git/qmk_firmware
git pull --recurse-submodules -j8
./util/qmk_install.sh
cd ~/git/qmk_firmware/keyboards/keebio/iris/keymaps/eidoom
git pull
```

### Build

```shell
cd ~/git/qmk_firmware
make keebio/iris/rev3:eidoom
```

### Flash

```shell
cd ~/git/qmk_firmware
make keebio/iris/rev3:eidoom:flash
```
then both thumbs on `Mod`s and `Tab` on the keyboard.

Note that `sudo` is required for the flash if [`udev` rules](https://docs.qmk.fm/#/faq_build?id=linux-udev-rules) have not been set.

## Resources

* [Build guide](https://docs.keeb.io/iris-rev3-build-guide/)
* [Testing](https://config.qmk.fm/#/test)
* [Key codes](https://docs.qmk.fm/#/keycodes)
